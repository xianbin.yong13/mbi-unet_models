from __future__ import absolute_import
from setuptools import setup, find_packages
from os import path

_dir = path.abspath(path.dirname(__file__))

with open(path.join(_dir,'unet_models','version.py')) as f:
    exec(f.read())

with open(path.join(_dir,'README.md')) as f:
    long_description = f.read()

setup(
    name="unet_models",
    version=__version__,
    author="Xianbin Yong",
    author_email="e0357915@u.nus.edu",
    description="Unet models for the MBI community",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/xianbin.yong13/mbi-unet_models",
    packages=find_packages(
		exclude=[
            "*.tests",
            "*.tests.*",
            "tests.*",
            "tests",
            "log",
            "log.*",
            "*.log",
            "*.log.*"
        ]),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
	install_requires=[
          "numpy",
          "scipy",
          "matplotlib",
          "six",
          "keras>=2.1.2",
          "h5py",
          "tifffile",
          "tqdm",
      ],
)