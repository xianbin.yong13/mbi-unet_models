Unet models for the MBI community

## Build setup

Make sure you have Python 3 and appropriate env.

    python --version

Setup if not already

```
sudo python -m pip install --upgrade pip setuptools wheel
sudo python -m pip install tqdm
sudo python -m pip install --user --upgrade twine
```

## Build the package

Update `version` in setup.py as necessary.

Now package

    python setup.py bdist_wheel

Note that `whl` Python package file is generated in `dist` folder.

Instead of publishing to public, we will just upload the `whl` file to local intranet

    scp dist/unet_models-0.0.2-py3-none-any.whl itcore:/var/www/itcore/mbi-doc/ai/files/

Also upload as latest

    scp dist/unet_models-0.0.2-py3-none-any.whl itcore:/var/www/itcore/mbi-doc/ai/files/unet_models-latest-py3-none-any.whl

To upload examples

    zip unet_example.zip -r examples
    scp unet_example.zip itcore:/var/www/itcore/mbi-doc/ai/files/

## Install

Note the generate file in `dist` folder and then install

    python -m pip install dist/unet_models-0.0.1-py3-none-any.whl

Or install from intranet

   pip install https://itcore.mbi.nus.edu.sg/mbi-doc/ai/files/unet_models-0.0.2-py3-none-any.whl



